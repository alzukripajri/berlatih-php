<!DOCTYPE html>
<html>
<head>
	<title>Tugas 3 PHP</title>
</head>
<body>


<?php 

echo "<h2> 3. Tukar Besar Kecil </h2>";

/*buatlah sebuah file dengan nama tukar-besar-kecil.php. Di dalam file tersebut buatlah function dengan nama tukar_besar_kecil yang menerima parameter berupa string. function akan mengembalikan sebuah string yang sudah ditukar ukuran besar dan kecil per karakter yang ada di parameter. Contohnya jika parameter “pHp” maka akan mengembalikan “PhP”.*/
	
function tukar_besar_kecil($string){
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $output = "";

    for ($i=0; $i < strlen($string); $i++) { 
        if (ctype_upper($string[$i])) {
            $output .= strtolower($string[$i]);
            # code...
        }else {
            $output .= strtoupper($string[$i]);
        }
    }

    return $output . "<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>


</body>
</html>